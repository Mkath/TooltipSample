﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Com.Tomergoldst.Tooltips;

namespace TooltipSample.Droid
{
    public sealed class TooltipCustom : ImageView, ToolTipsManager.ITipListener
    {
        ImageView image;

        private string Tag = nameof(MainActivity);
        private readonly Context _context;
        private readonly ToolTipsManager _toolTipsManager;
        ToolTip.Builder tooltip;
        ViewGroup viewGroup;

        String title;

        public TooltipCustom(Context context, ViewGroup viewGroup, ImageView image, String texto) : base(context)
        {
            this._context = context;
            this.viewGroup = viewGroup;
            this.image = image;
            title = texto;

            _toolTipsManager = new ToolTipsManager(this);
        
            
        }
        public override void OnWindowFocusChanged(bool hasFocus)
        {
            base.OnWindowFocusChanged(hasFocus);

            ToolTip.Builder builder = new ToolTip.Builder(_context, image, viewGroup,title, ToolTip.PositionAbove);
            builder.SetAlign(ToolTip.PositionAbove);
            _toolTipsManager.Show(builder.Build());
        }

        public void setTooltip()
        {
            tooltip = new ToolTip.Builder(_context, image, viewGroup, title, ToolTip.PositionAbove);
            tooltip.SetAlign(ToolTip.PositionAbove);
        
            _toolTipsManager.Show(tooltip.Build());
        }

        public void OnTipDismissed(View p0, int p1, bool p2)
        {
            Log.Debug(Tag, "tip near anchor view " + p1 + " dismissed");
        }
    }
}